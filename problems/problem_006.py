# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#original
# def can_skydive(age, has_consent_form):
#     can_skydive = True
#     if age < 18 or has_consent_form == False:
#         can_skydive = False
#     if can_skydive == True:
#         print("You can skydive!")
#     else:
#         print("sadly you cannot skydive :(!")

#refactored
def can_skydive(age, has_consent_form):
    if age >= 18 and has_consent_form:
        return True
    else:
        return False


#sample input
test = can_skydive(21, False)
print(test)
test2 = can_skydive(18, True)
print(test2)

#function takes in two parameters, age, and has_consent_form
#function asks if a person can skydive or not (should return True/False)
#can_skydive = True
#if age >= 18 or has_consent_form == True
#can_skydive = True
#else
#can_skydive = False

#if can_skydive = True
#print("person can skydive!")
