# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    if len(attendees_list) >= (len(members_list))/2:
        return True
    else:
        return False

test1 = has_quorum(['a','b','c'], ['a','b','c','d'])
test2 = has_quorum(['a'],['a', 'b', 'c', 'd', 'e', 'f'])
print("Should be true:",test1)
print("Should be false:", test2)


"""
if number of people in attendees list >= number of people in members list/2
return True
else
return False
"""
