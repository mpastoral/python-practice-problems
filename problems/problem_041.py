# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    result_list = []
    for item_list in csv_lines:
        item_list_split = item_list.split(",")
        line_sum = 0

        for item in item_list_split:
            item_value = int(item)
            line_sum += item_value

        result_list.append(line_sum)

    return result_list


"""
function list

    return new list that is sum of numbers in comma separated string

"""
test = add_csv_lines(["3", "1,9"])
print(test)

listex = ["1,2,3", "4,5,6"]
for i in listex:
    print("i:",i)
    j = i.split(",")
    print("j:",j)
