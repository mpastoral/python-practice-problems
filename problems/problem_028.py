# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    if len(s)==0:
        return ""
    unique_letters = []
    for letter in s:
        if letter not in unique_letters:
            unique_letters.append(letter)

    unique_letters = "".join(unique_letters)
    return unique_letters
    pass
test = remove_duplicate_letters('aaabbcccccdeefgh')
print(test)

"""
if len(s) == 0
return ""

unique_letters = []
for letter in s:
if letter not in string
unique_letters.append

turn list into a string
return string

"""
