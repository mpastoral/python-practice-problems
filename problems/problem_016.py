# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if (x >= 0 and x <= 10) and (y >=0 and y<=10):
        return True
    else:
        return False
    pass

#sample input
test = is_inside_bounds(10,1)
print(test)

#takes 2 parameters
#if x equal to or greater than 0 and x is equal or less than 10 and
# y equal to or greater than 0 and y is equal or less than 10:
#return True
#else
#return False
