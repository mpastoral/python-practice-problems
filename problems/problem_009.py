# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    reversed_word_list = reversed(word)
    reversed_word = "".join(reversed_word_list)
    if word == reversed_word:
        return True

word = "ho-oh"
print(f"{word} is a palindrome:", is_palindrome(word))

#reversed(string) returns a list, can't print it
#use .join to turn reversed(string) into a printable string
#if string == reversed_string
#print("it is a palindrome!")
#sample input
