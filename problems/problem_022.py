# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    gear_list = []
    if is_sunny == False and is_workday == True:
        gear_list.append("umbrella")

    if is_workday == True:
        gear_list.append("laptop")

    if is_workday == False:
        gear_list.append("surfboard")

    return gear_list

test = gear_for_day(True, False)
print(test)




"""
wrong solution before actually reading problem LOL
if sunny == False AND workday == True:
    if "umbrella" not in gear_list:
        print("You need to bring an umbrella")
    else:
        print("You have an umbrella.")

if workday == True:
    if "laptop" not in gear_list:
        print("You need a laptop!")
    else:
        print("You have your laptop.")
if workday == False:
    if surfboard not in gear_list:
        print("You need to bring your surfboard!")
    else:
        print("You got your surfboard!")
"""

#sample input
test = gear_for_day
