# Write a function that meets these requirements.
#
# Name:       remove_duplicates
# Parameters: a list of values
# Returns:    a copy of the list removing all
#             duplicate values and keeping the
#             original order
#
# Examples:
#     * input:   [1, 1, 1, 1]
#       returns: [1]
#     * input:   [1, 2, 2, 1]
#       returns: [1, 2]
#     * input:   [1, 3, 3, 20, 3, 2, 2]
#       returns: [1, 3, 20, 2]

def remove_duplicates(list_val):
    # new_val = []
    # for i in list_val:
    #     if i not in new_val:
    #         new_val.append(i)
    # new_val = list(set(tuple(list_val))) #set/tuple does not work because it does not keep original order
    return new_val  #copy of list_val removing duplicate values and keeping same order

test1 = remove_duplicates([1,1,1,1])
test2 = remove_duplicates([1,3,3,20,3,2,2])

print(test1)
print(test2)