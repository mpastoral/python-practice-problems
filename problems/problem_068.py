# Write a class that meets these requirements.
#
# Name:       Person
#
# Required state:
#    * name, a string
#    * hated foods list, a list of names of food they don't like
#    * loved foods list, a list of names of food they really do like
#
# Behavior:
#    * taste(food name)  * returns None if the food name is not in their
#                                  hated or loved food lists
#                        * returns True if the food name is in their
#                                  loved food list
#                        * returns False if the food name is in their
#                                  hated food list
#

class Person:
    def __init__(self,name, hated_foods, loved_foods):
        self.name = name
        self.hated_foods = hated_foods
        self.loved_foods = loved_foods

    def taste(self, food):
        if food not in self.hated_foods and food not in self.loved_foods:
            return None
        elif food in self.hated_foods:
            return False
        else:
            return True

yukari = Person('Yukari', ['apple', 'asparagus', 'apricots'],['candy', 'cake', 'chocolate'])
print("Should return None:",yukari.taste('chicken'))
print("Should return False:",yukari.taste('apple'))
print("Should return True:",yukari.taste('cake'))


    #initialize states name hatedlist lovedlist
    #define behaviors
    #def taste food
        #if food is in neither hated or loved list, return none
        #if food is in person's hated list, return false
        #else
        #if food is in person's loved list, return true





















# Example:
#    person = Person("Malik",
#                    ["cottage cheese", "sauerkraut"],
#                    ["pizza", "schnitzel"])
#
#    print(person.taste("lasagna"))     # Prints None, not in either list
#    print(person.taste("sauerkraut"))  # Prints False, in the hated list
#    print(person.taste("pizza"))       # Prints True, in the loved list

# class Person
    # method initializer with name, hated foods list, and loved foods list
        # self.name = name
        # self.hated_foods = hated_foods
        # self.loved_foods = loved_foods
    # method taste(self, food)
        # if food is in self.hated_foods
            # return False
        # otherwise, if food is in self.loved_foods
            # return True
        # otherwise
            # return None
