# Write a function that meets these requirements.
#
# Name:       basic_calculator
# Parameters: left, the left number
#             op, the math operation to perform
#             right, the right number
# Returns:    the result of the math operation
#             between left and right
#
# The op parameter can be one of four values:
#   * "+" for addition
#   * "-" for subtraction
#   * "*" for multiplication
#   * "/" for division
#
# Examples:
#     * inputs:  10, "+", 12
#       result:  22
#     * inputs:  10, "-", 12
#       result:  -2
#     * inputs:  10, "*", 12
#       result:  120
#     * inputs:  10, "/", 12
#       result:  0.8333333333333334
import operator

def basic_calculator(left, op, right):
    op_values = {
        "+": operator.add(left, right),
        "-": operator.sub(left, right),
        "*": operator.mul(left, right),
        "/": operator.truediv(left, right)}
    
    result = None
    if op not in op_values:
        return ValueError
    
    if op in op_values:
        result =  op_values[op]
    return result #math op between left and right

test1 = basic_calculator(10, "+", 12)

print(test1)