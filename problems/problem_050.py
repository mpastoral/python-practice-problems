# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

#solution 1
# def halve_the_list(list):
#     first_half = list[:(len(list)//2)+1]
#     second_half = list[(len(list)//2)+1:]

#     return first_half, second_half


# print(listex[:len(listex)//2])
# print(listex[:len(listex)//2]:)


#solution 2
def halve_the_list(list):
    first_half = []
    second_half = []
    first_half_len = len(list)//2 + (len(list)%2)
    for i in range(first_half_len): #represents going through the indexes of a list's length using range
        first_half.append(list[i]) #list[i] represents the element in list, not the index
    for i in range(len(list)//2):
        index = i + first_half_len
        second_half.append(list[index])

    return first_half, second_half

test = halve_the_list([0,1,2,3,4])
print(test)
