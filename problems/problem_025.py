# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if len(values) == 0:
        return None

    sum = 0
    for num in values:
        sum += num

    return sum

test = calculate_sum([4,3,6])
print("Should be 13:", test)

"""
if len list values = 0
return None

sum = 0

for num in values:
sum+=num

return sum

"""
