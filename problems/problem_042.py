# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.

def pairwise_add(list1, list2):
    list3 = []
    for i in range(len(list1)):
        sum = list1[i] + list2[i]
        list3.append(sum)
    return list3

test1 = (pairwise_add([1, 2, 3, 4],[4, 5, 6, 7]))
test2 = (pairwise_add([100,200,300],[10,1,100]))

print(test1)
print(test2)

"""
function list1 list2
    list3 = []
    for i in range(len(list1)):
        sum = list1[i] + list2[i]
        list3.append(sum)
    return new list with each index a sum of the index of the two lists

"""
