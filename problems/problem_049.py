# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#

def sum_two_numbers(num_one, num_two):
    return num_one + num_two


test = sum_two_numbers(3, 4)
print(test)

# Examples:
#    * x: 3
#      y: 4
#      result: 7
