# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

def pad_left(number, length, pad):
    number = str(number)
    pad_multiplier = length - len(number)
    num_of_pad = pad_multiplier*pad
    result = num_of_pad + number
    return result

test1 = pad_left(10, 4, "*")
test2 = pad_left(10, 5, "0")
test3 = pad_left(1000, 3, "0")
test4 = pad_left(19, 4, " ")

print(test1)
print(test2)
print(test3)
print(test4)



"""
function name parameters
    want to add char to number so that it has a length of the given length
    pad_multiplier = length - len(string)
    num_of_pad = pad_multiplier*pad
    result = num_of_pad + str(number)
    return result


"""
