# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    sum = 0
    length = len(values)
    if len(values) == 0:
        return None

    for num in values:
        sum += num
    average = sum/length

    return int(average)

#sample input
test = calculate_average([5, 3, 10])
print(test)

test2 = calculate_average([3,3,3])
print(test2)




"""
if list values is empty
return None

sum = 0
length = len(values)

for number in values:
add number to sum

average = sum divided by length
return average
"""
