# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    minimum = 0
    if value1 < value2:
        minimum = int(value1)
        return minimum
    elif value1 > value2:
        minimum = int(value2)
        return minimum
    else:
        print("No minimum, they are the same value")

print(minimum_value(5, 10))

#using if to compare value 1 and value 2
# assign variable to smaller of the two values
# if equal, print ("they are equal")
#make sure they're integers or floats

#minimum variable name

#if value1 is smaller than value2
#minimum = int(value1)

#elif value2 is smaller than value1
#minimum = int(value2)
#else
#print they are the same value
#return minimum
