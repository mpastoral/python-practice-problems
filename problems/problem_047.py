# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    has_lowercase = False
    has_uppercase = False
    has_digit = False
    has_special_character = False
    special_characters = ["$", "!", "@"]
    at_least_six = False
    at_most_twelve = False

    for char in password:
        if char.isalpha():
            if char.islower():
                has_lowercase = True
            if char.isupper():
                has_uppercase = True
        elif char.isdigit():
            has_digit = True
        elif char in special_characters:
            has_special_character = True
        else:
            print(f"This character {char} can't be used.")
            return False
    if len(password) >= 6 and len(password) <= 12:
        at_least_six = True
        at_most_twelve = True
    criteria = [has_lowercase,has_uppercase,has_digit,has_special_character,at_least_six,at_most_twelve]
    for crit in criteria:
        print(crit)
        if crit == False:
            return False
    return True



    # return any(criteria)
    # if has_lowercase == True and has_uppercase == True and has_one_digit ==
    # True and has special_characters == True:

test = check_password("Aa1A!bc3")
print(test)









"""
has lowercase = false
has uppercase = false
has one digit = false
has special character = false

for char in password:
    if char.isalpha():
        if char.islower:



"""
