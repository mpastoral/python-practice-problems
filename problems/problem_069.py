# Write a class that meets these requirements.
#
# Name:       Student
#
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
#    * get_average()      # Gets the average of the student's scores

class Student:
    def __init__(self, name, score_list = []):
        self.name = name
        self.score_list = score_list

    def add_score(self, score):
        self.score_list.append(score)
        return self.score_list

    def get_average(self):
        sum = 0
        length = len(self.score_list)
        for score in self.score_list:
            sum += score
        average = sum/length
        return average

#sample input

junpei = Student('Junpei')
print(junpei.add_score(60))
print(junpei.add_score(50))
print("Should be 55:",junpei.get_average())

# class Student:
    #initialize w/ name
    #self.name = name
    #self.score_list = list

    #def add_score(self, score)
        #list.append(score)
        #return list
    #def get_average(self)
        #get total scores of list
"""
            sum = 0
            length = len(self.score_list)
            loop through score_list
            sum = sum + each score
            avg = sum/length
            return avg
            """
        #get length of list
        #get average
        #return average




















#
# Example:
#    student = Student("Malik")
#
#    print(student.get_average())    # Prints None
#    student.add_score(80)
#    print(student.get_average())    # Prints 80
#    student.add_score(90)
#    student.add_score(82)
#    print(student.get_average())    # Prints 84
#
# There is pseudocode for you to guide you.

# class Student
    # method initializer with required state "name"
        # self.name = name
        # self.scores = [] because its an internal state

    # method add_score(self, score)
        # appends the score value to self.scores

    # method get_average(self)
        # if there are no scores in self.scores
            # return None
        # returns the sum of the scores divided by
        # the number of scores
