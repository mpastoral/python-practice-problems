# Write a function that meets these requirements.
#
# Name:       generate_lottery_numbers
# Parameters: none
# Returns:    a list of six random unique numbers
#             between 1 and 40, inclusive
#
# Example bad results:
#    [4, 2, 3, 3, 1, 5] duplicate numbers
#    [1, 2, 3, 4, 5] not six numbers
#
# You can use randint from random, here, or any of
# the other applicable functions from the random
# package.
#
# https://docs.python.org/3/library/random.html
import random

def generate_lottery_numbers():
    #make new list
    #while the length of the new list is less than 6:
    #number = random.randint(1,40)
    numbers = []
    while len(numbers) < 6:
        number = random.randint(1,40)
        numbers.append(number)

    return numbers

def fun():
    list = [1,2,3]
    for i in list:
        return "test"

def generate_lotter_numbers_alt():
    numbers = list(range(1,41))
    new_list = []
    random.shuffle(numbers)
    for i in range(6):
        new_list.append(numbers[i])
    return new_list

#solution two
# def generate_lottery_numbers():
#    numbers = list(range(1,41))
#    new_list = []
#    random.shuffle(numbers)
#    for i in range(6):

#     # new_list.append(numbers[i])

#     return new_list




test = generate_lottery_numbers()
testalt = generate_lotter_numbers_alt()
print(test)
print(testalt)
