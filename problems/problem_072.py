# Write a class that meets these requirements.
#
# Name:       Person
#
# Required state:
#    * name, a string
#    * hated foods list, a list of names of food they don't like
#    * loved foods list, a list of names of food they really do like
#
# Behavior:
#    * taste(food name)  * returns None if the food name is not in their
#                                  hated or loved food lists
#                        * returns True if the food name is in their
#                                  loved food list
#                        * returns False if the food name is in their
#                                  hated food list
#
# Example:
#    person = Person("Malik",
#                    ["cottage cheese", "sauerkraut"],
#                    ["pizza", "schnitzel"])
#
#    print(person.taste("lasagna"))     # Prints None, not in either list
#    print(person.taste("sauerkraut"))  # Prints False, in the hated list
#    print(person.taste("pizza"))       # Prints True, in the loved list
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

class Person:
    def __init__(self,name,hated,loved):
        self.name = name
        self.hated = hated
        self.loved = loved

    def taste(self, food):
        food = food.lower()
        if food not in self.hated and food not in self.loved:
            return None
        elif food in self.hated and food not in self.loved:
            return False
        elif food in self.loved and food not in self.hated:
            return True
        else:
            print("Food cannot be in both hated and loved lists!")

akihiko = Person('Akihiko', ['cake', 'white bread', 'donuts'], ['chicken', 'sashimi', 'KBBQ'])

print("Should be None:",akihiko.taste('soju'))
print("Should be False:",akihiko.taste('cake'))
print("Should be True:",akihiko.taste('Sashimi')) #test for uppercase
