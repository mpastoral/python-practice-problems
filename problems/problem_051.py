# Write a function that meets these requirements.
#
# Name:       safe_divide
# Parameters: two values, a numerator and a denominator
# Returns:    if the denominator is zero, then returns math.inf.
#             otherwise, returns numerator / denominator
#
# Don't for get to import math!
import math

def safe_divide(numerator, denominator):
    if denominator == 0:
        return math.inf
    else:
        return numerator/denominator

test = safe_divide(4,5)
test2 = safe_divide(1,0)
print(test)
print(test2)
