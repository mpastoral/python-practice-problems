# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1
#
# To test if a character c is a digit, you can use the
# c.isdigit() method to return True of False
#
# To test if a character c is a letter, you can use the
# c.isalpha() method to return True of False
#
# Remember that functions can return more than one value
# in Python. You just use a comma with the return, like
# this:
#      return value1, value2


def count_letters_and_digits(string):
    # string = str(string)
    num_letters = 0
    num_digits = 0
    for i in string:
        if i.isalpha():
            num_letters += 1
        if i.isdigit():
            num_digits += 1
    return num_letters, num_digits

test1 = count_letters_and_digits('1')
test2 = count_letters_and_digits('a')
test3 = count_letters_and_digits('1a')
test4 = count_letters_and_digits('38a84b')

print(test1)
print(test2)
print(test3)
print(test4)
#pseudocode
"""
def count_letters_and_digits(string):
    num_letters = 0
    num_digits = 0
    for i in string:
        if i.isdigit() is true:
            num_digits += 1
        if i.isalpha() is true:
            num_letters += 1
    return num_letters, num_digits

"""
