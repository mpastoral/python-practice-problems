# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) < 2:
        return None
    max = values[0]
    secondmax = values[1]
    for num in values:
        if num >= max:
            secondmax = max
            max = num
        elif num >= secondmax:
            secondmax = num
    return secondmax


test = find_second_largest([5,6,10])
test2 = find_second_largest([5,6,5.9])

print(test)
print(test2)




"""
if len list == 0 or len list == 1
return None

max = ""
secondmax = ""

for num in values:
if num >= max
max = num

for num in values:
if num >= secondmax and num != max:
secondmax = num

return secondmax


return second largest number in list
"""
