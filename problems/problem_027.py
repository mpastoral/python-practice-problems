# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    max = 0
    for num in values:
        if num >= max:
            max = num

    return max

test = max_in_list([4,3,10])
test2 = max_in_list([5,3,3])
print("should be 10:", test)
print("should be 5:", test2)

"""
max = ""
for num in values
if num >= max
max = num

return max
"""
